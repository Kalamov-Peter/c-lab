﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Практика
{
    class Program
    {
        change help = new change();
        static void Main(string[] args)
        {
            int choose = 0;
            Program func = new Program();
            Console.WriteLine("Menu:");
            Console.WriteLine("1)Add contact");
            Console.WriteLine("2)Edit contact");
            Console.WriteLine("3)Delete contact");
            Console.WriteLine("4)List of all contacts");
            Console.WriteLine("5)View contact info");
            Console.WriteLine("6)Exit");
            while (choose != 6)
            {
                Console.WriteLine("Enter the command number");
                choose = int.Parse(Console.ReadLine());
                func.DoCommand(choose);
            }
        }
        void DoCommand(int command)
        {
            string surname1 = null;
            string name1 = null;
            string patronymic1 = null;
            string number1 = null;
            string birthday1 = null;
            string country1 = null;
            string company1 = null;
            string post1 = null;
            string other1 = null;
            switch (command)
            {
                case 1:
                    Console.WriteLine("Input surname");
                    surname1 = Console.ReadLine();
                    Console.WriteLine("Input name");
                    name1 = Console.ReadLine();
                    Console.WriteLine("Input patronymic");
                    patronymic1 = Console.ReadLine();
                    Console.WriteLine("Input number");
                    number1 = Console.ReadLine();
                    Console.WriteLine("Input country");
                    country1 = Console.ReadLine();
                    Console.WriteLine("Input birthday");
                    birthday1 = Console.ReadLine();
                    Console.WriteLine("Input company");
                    company1 = Console.ReadLine();
                    Console.WriteLine("Input post");
                    post1 = Console.ReadLine();
                    Console.WriteLine("Input other");
                    other1 = Console.ReadLine();
                    help.input(surname1, name1, patronymic1, number1, country1, birthday1, company1, post1, other1);
                    break;
                case 2:
                    string item = null;
                    Console.WriteLine("Choose the item what you want to edit\nName --- name of contact\nSurname --- surname of contact\nPatronymic --- patronymic of contact\nPhone --- phone number  of contact\nBirth --- birthaday of contact\nComp --- company of contact\nPost --- post of contact\nOther --- other info  about contact ");
                    item = Console.ReadLine();
                    if (AllConditions() != null)
                    {
                        Item(item.ToUpper(), AllConditions());
                    }
                    break;
                case 3:
                    if (AllConditions() != null)
                    {
                        help.delete(AllConditions());
                    }
                    break;
                case 4:
                    Console.WriteLine("\nList of all contacts ");
                    help.AllOut(delegate (about a)
                    {
                        Console.WriteLine("\n   Name - {0}\n   Surname - {1}\n   Phone number - {2}\n", a.name, a.surname, a.number);
                    });
                    break;
                case 5:
                    about show = AllConditions();
                    if (show == null)
                    {
                        Console.WriteLine("There is no contact with such data");
                    }
                    else
                    {
                        Console.WriteLine("\n   Name - {0}\n   Surname - {1}\n   Patronymic - {2}\n   Phone number - {3}\n   Country - {4}\n   Birthday - {5}\n   Company - {6}\n   Post - {7}\n   Other info - {8}\n", show.name, show.surname, show.patronymic, show.number, show.country, show.birthday, show.company, show.post, show.other);
                    }
                    break;
            }
        }
        void Item(string item, about time)
        {
            string rename;
            Console.WriteLine("Enter the new data");
            rename = Console.ReadLine();
            switch (item)
            {
                case "NAME":
                    time.name = rename;
                    Console.WriteLine("Data was updated");
                    break;
                case "SURNAME":
                    time.surname = rename;
                    Console.WriteLine("Data was updated");
                    break;
                case "PATRONYMIC":
                    time.patronymic = rename;
                    Console.WriteLine("Data was updated");
                    break;
                case "PHONE":
                    time.number = rename;
                    Console.WriteLine("Data was updated");
                    break;
                case "BIRTH":
                    time.birthday = rename;
                    Console.WriteLine("Data was updated");
                    break;
                case "COMP":
                    time.company = rename;
                    Console.WriteLine("Data was updated");
                    break;
                case "POST":
                    time.post = rename;
                    Console.WriteLine("Data was updated");
                    break;
                case "OTHER":
                    time.other = rename;
                    Console.WriteLine("Data was updated");
                    break;
                case "":
                    Console.WriteLine("Data will not be changed");
                    break;

            }
        }
        about AllConditions()
        {
            string name = null;
            string surname = null;
            string birthday = null;
            Console.WriteLine("Enter the name of the contact you are looking for");
            name = Console.ReadLine();
            if (help.CheckNameDiff(name))
            {
                return help.UseNameDiff(name);
            }
            else
            {
                Console.WriteLine("Enter the surname of the contact you are looking for");
                surname = Console.ReadLine();
                if (help.CheckSurnameDiff(name, surname))
                {
                    return help.UseSurnameDiff(name, surname);
                }
                else
                {
                    Console.WriteLine("Enter the birthday of the contact you are looking for");
                    birthday = Console.ReadLine();
                    if (help.CheckBirthdayDiff(name, surname, birthday))
                    {
                        return help.UseBirthdayDiff(name, surname, birthday);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

        }
    }
    class about
    {
        public string surname;
        public string name;
        public string patronymic;
        public string number;
        public string country;
        public string birthday;
        public string company;
        public string post;
        public string other;
        public about(string surname1, string name1, string patronymic1, string number1, string country1, string birthday1, string company1, string post1, string other1)
        {
            surname = surname1;
            name = name1;
            patronymic = patronymic1;
            number = number1;
            country = country1;
            birthday = birthday1;
            company = company1;
            post = post1;
            other = other1;
        }
        public about()
        {

        }
    }
    class change
    {
        public List<about> roster = new List<about>();
        public void input(string surname1, string name1, string patronymic1, string number1, string country1, string birthday1, string company1, string post1, string other1)
        {
            about contact = new about(surname1, name1, patronymic1, number1, country1, birthday1, company1, post1, other1);
            roster.Add(contact);
        }
        public bool CheckNameDiff(string name)
        {
            about check1 = roster.Find(delegate (about x)
            {
                return x.name == name;
            });
            about check2 = roster.FindLast(delegate (about x)
            {
                return x.name == name;
            });
            if (check1 == check2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public about UseNameDiff(string name)
        {
            about check = roster.Find(delegate (about x)
            {
                return x.name == name;
            });
            return check;
        }
        public bool CheckSurnameDiff(string name, string surname)
        {
            about check1 = roster.Find(delegate (about x)
            {
                return (x.name == name && x.surname == surname);
            });
            about check2 = roster.FindLast(delegate (about x)
            {
                return (x.name == name && x.surname == surname);
            });
            if (check1 == check2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public about UseSurnameDiff(string name, string surname)
        {
            about check = roster.Find(delegate (about x)
            {
                return (x.name == name && x.surname == surname);
            });
            return check;
        }
        public bool CheckBirthdayDiff(string name, string surname, string birthday)
        {
            about check1 = roster.Find(delegate (about x)
            {
                return (x.name == name && x.surname == surname && x.birthday == birthday);
            });
            about check2 = roster.FindLast(delegate (about x)
            {
                return (x.name == name && x.surname == surname && x.birthday == birthday);
            });
            if (check1 == check2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public about UseBirthdayDiff(string name, string surname, string birthday)
        {
            about check = roster.Find(delegate (about x)
            {
                return (x.name == name && x.surname == surname && x.birthday == birthday);
            });
            return check;
        }
        public void delete(about time)
        {
            roster.Remove(time);
        }
        public void AllOut(Action<about> OUT)
        {
            roster.ForEach(OUT);
        }
    }
}
